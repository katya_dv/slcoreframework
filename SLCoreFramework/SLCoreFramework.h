//
//  SLCoreFramework.h
//  SLCoreFramework
//
//  Created by Ekaterina on 2020-05-18.
//  Copyright © 2020 SL. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for SLCoreFramework.
FOUNDATION_EXPORT double SLCoreFrameworkVersionNumber;

//! Project version string for SLCoreFramework.
FOUNDATION_EXPORT const unsigned char SLCoreFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SLCoreFramework/PublicHeader.h>



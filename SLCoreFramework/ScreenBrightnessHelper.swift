//
//  ScreenBrightnessHelper.swift
//  SLCoreFramework
//
//  Created by Ekaterina on 2020-05-18.
//  Copyright © 2020 SL. All rights reserved.
//

import UIKit

public class ScreenBrightnessHelper {

    fileprivate static let brightnessQueue = DispatchQueue(label: "brightnessQueue")
    
   public static func maxBrightness() {
        #if targetEnvironment(simulator)
        #else
        DispatchQueue.main.async {
            let currentBrightness = UIScreen.main.brightness
            if currentBrightness != 1.0 {
                var brightness = UIScreen.main.brightness
                while brightness < 1.0 {
                    UIScreen.main.brightness += 0.01
                    brightness = UIScreen.main.brightness
                    usleep(5)
                }
            }
        }
        #endif
    }

    public static func restoreBrightness(dimEffect: Bool = true) {
        #if targetEnvironment(simulator)
        #else
        DispatchQueue.main.async {
            guard let userSetBrightness = UserDefaults.standard.object(forKey: "deviceBrightness") as? CGFloat  else {
                return
            }
            var brightness = UIScreen.main.brightness
            if dimEffect {
                while brightness > userSetBrightness {
                    UIScreen.main.brightness -= 0.01
                    brightness = UIScreen.main.brightness
                    usleep(5)
                }
            } else {
                UIScreen.main.brightness = userSetBrightness
            }
        }
        #endif
    }

    public static func saveBrightness() {
        let currentBrightness = UIScreen.main.brightness
        UserDefaults.standard.set(currentBrightness, forKey: "deviceBrightness")
    }
}
